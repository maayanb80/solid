import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VolunteerManagerComponent} from "./volunteerManager/volunteerManager.component";
import {SignInComponent} from "./signIn/signIn.component";
import {AdminPageComponent} from './admin/adminPage.component';
import {NewTaskComponent} from './tasks/newTask.component';

const appRoutes: Routes = [
    {path: '', redirectTo: '/signIn', pathMatch: 'full'},
    {path: 'signIn', component: SignInComponent},
    {path: 'volunteerManager', component: VolunteerManagerComponent},
    // { path: 'detail/:id', component: HeroDetailComponent }
    {path: 'adminPage', component: AdminPageComponent},
    {path: 'newTask', component: NewTaskComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true} // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
