import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {SignInComponent} from "./signIn.component";

@NgModule({
    declarations: [
        SignInComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        NgbModule.forRoot()
    ],
    providers: [],
    bootstrap: [SignInComponent]
})

export class SignInModule { }
