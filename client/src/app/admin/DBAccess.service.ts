import {Injectable} from '@angular/core';
import {HttpClient, HttpClientModule, HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export class DBAccessService {
    constructor(private http: HttpClient) {}

    private options: {
        headers?: HttpHeaders | { [header: string]: string | string[] },
        observe?: 'body',
        params?: HttpParams | { [param: string]: string | string[] },
        reportProgress?: boolean,
        responseType?: 'json',
        withCredentials?: boolean,
    };

    public addNewTask(taskInfo) {
        const body = {
            "values": taskInfo
        };
        this.http.post('localhost:8080/tasks', body, this.options);
    }

    public getTasks(taskInfo): Promise<string> {
        const tasks = {
            "values": {
                "name": "sfsdf",
                "details": "sdsd",
                "source_address": "sdgds",
                "destination_adrress": "sd",
                "group_id": "3",
                "area_id": "5",
                "first_contact_name": "dsfs",
                "first_contact_phone": "sdf"
            }
        };
        // this.http.post('localhost:8080/tasks', body, this.options);
        return new Promise<string>(resolve => tasks);
    }
}
