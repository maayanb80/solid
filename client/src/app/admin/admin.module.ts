import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {AdminPageComponent} from './adminPage.component';
import {RouterModule} from '@angular/router';
import {NewTaskComponent} from '../tasks/newTask.component';
// import {MatIconModule} from '@angular/material/icon';

@NgModule({
    declarations: [
        AdminPageComponent,
        NewTaskComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        NgbModule.forRoot(),
        RouterModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AdminPageComponent, NewTaskComponent]
})

export class AdminPageModule { }
