import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './adminPage.component.html',
    styleUrls: ['./adminpage.component.css']
})
export class AdminPageComponent {
    appTitle = 'hello world';

    myFunc() {
        alert('func');
    }
}
