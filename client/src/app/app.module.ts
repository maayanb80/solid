import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from "./app-routes";
import {VolunteerManagerModule} from "./volunteerManager/volunteerManager.module";
import {SignInModule} from "./signIn/signIn.module";
import {RouterModule} from '@angular/router';
import {AdminPageModule} from './admin/admin.module';
import {DBAccessService} from "./admin/DBAccess.service";
import {CategoryPickerModule} from "./categoryPicker/categoryPicker.module";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        AppRoutingModule,
        VolunteerManagerModule,
        SignInModule,
      CategoryPickerModule,
    NgbModule.forRoot(),
        AdminPageModule,
        RouterModule,
        AppRoutingModule
    ],
    providers: [DBAccessService],
    bootstrap:
        [AppComponent]
})

export class AppModule {
}
