import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {CategoryPickerComponent} from "./categoryPicker.component";

@NgModule({
    declarations: [
        CategoryPickerComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        NgbModule.forRoot()
    ],
    providers: [],
    bootstrap: [CategoryPickerComponent]
})

export class CategoryPickerModule { }
