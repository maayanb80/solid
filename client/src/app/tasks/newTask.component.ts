import {Component} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import { DBAccessService } from '../admin/DBAccess.service';

@Component({
    selector: 'app-root',
    templateUrl: './newTask.component.html',
    styleUrls: ['./newTask.component.css']
})
export class NewTaskComponent {
    appTitle = 'hello world';
    categories: number[] = [1, 2, 3];
    areas: number[] = [4, 5, 6];
    checkoutForm;

    constructor(
        private formBuilder: FormBuilder,
        private dbaccessService: DBAccessService
    ) {
        this.checkoutForm = this.formBuilder.group({
            name: '',
            details: '',
            source_address: '',
            destination_adrress: '',
            group_id: '',
            area_id: '',
            first_contact_name: '',
            first_contact_phone: ''
        });
    }

    createTask(newTaskParams) {
        this.dbaccessService.addNewTask(newTaskParams);
    }
}
