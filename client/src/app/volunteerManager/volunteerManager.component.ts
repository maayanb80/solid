import { Component } from '@angular/core';

@Component({
    selector: 'volunteer-manager',
    templateUrl: './volunteerManager.html',
    styleUrls: ['./volunteerManager.css']
})
export class VolunteerManagerComponent {
    title = 'volunteer Manager';
}
