const db = require('../repositories/db');

const getAllTasks = (req, res) => {
    db.Get(req, res, 'tasks');
};

const getTaskById = (req, res) => {
    db.GetById(req, res, 'tasks', req.params.id);
};

const addTask = (req, res) => {
    db.Add(req, res, 'tasks', req.body.values);
};

const updateTask = (req, res) => {
    db.Put(req, res, 'tasks', req.params.id, req.body.values);
};

const getTasksByUser = (req, res) => {
    const query = 'SELECT * FROM tasks WHERE id IN (SELECT id FROM users WHERE id = ' + 
        req.params.id + 
    ');';

    db.excQuery(req, res, query);
}

const getTasksByFilter = (req, res) => {
    db.GetWithFliter(req, res, 'tasks', req.params.key, req.params.value)
}

module.exports = {
    getAllTasks,
    getTaskById,
    addTask,
    updateTask,
    getTasksByUser,
    getTasksByFilter
}