const db = require('../repositories/db');

const getAllGroups = async (req, res) => {
    db.Get(req, res, 'groups');
};

const getGroupById = async (req, res) => {
    db.GetById(req, res, 'groups', req.params.id);
};

const addGroup = async (req, res) => {
    db.Add(req, res, 'groups', req.body.values);
};

const updateGroup = async (req, res) => {
    db.Put(req, res, 'groups', req.params.id, req.body.values);
};

const getGroupsByFilter = (req, res) => {
    db.GetWithFliter(req, res, 'groups', req.params.key, req.params.value)
}

module.exports = {
    getAllGroups,
    getGroupById,
    addGroup,
    updateGroup,
    getGroupsByFilter
}