const db = require('../repositories/db');

const getAllUsers = (req, res) => {
    db.Get(req, res, 'users');
};

const getUserById = (req, res) => {
    db.GetById(req, res, 'users', req.params.id);
};

const addUser = (req, res) => {
    db.Add(req, res, 'users', req.body.values);
};

const updateUser = (req, res) => {
    db.Put(req, res, 'users', req.params.id, req.body.values);
};

const getUsersByGroup = (req, res) => {
    const query = 'SELECT * FROM users WHERE id IN (SELECT id FROM team_members WHERE group_id = ' + 
        req.params.id + 
    ');';

    db.excQuery(req, res, query);
}

const getUsersByFilter = (req, res) => {
    db.GetWithFliter(req, res, 'users', req.params.key, req.params.value)
}

module.exports = {
    getAllUsers,
    getUserById,
    addUser,
    updateUser,
    getUsersByGroup,
    getUsersByFilter
}