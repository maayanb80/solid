const groups = require('./groups');
const area = require('./area');
const users = require('./users');
const tasks = require('./tasks');
const status = require('./status');

module.exports = {
    groups,
    area,
    users,
    tasks,
    status
}