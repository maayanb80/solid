const db = require('../repositories/db');

const getAllAreas = async (req, res) => {
    db.Get(req, res, 'area');
};

const getAreaById = async (req, res) => {
    db.GetById(req, res, 'area', req.params.id);
};

const addArea = async (req, res) => {
    db.Add(req, res, 'area', req.body.values);
};

const updateAarea = async (req, res) => {
    db.Put(req, res, 'area', req.params.id, req.body.values);
};

const getAreasByFilter = (req, res) => {
    db.GetWithFliter(req, res, 'area', req.params.key, req.params.value)
}

module.exports = {
    getAllAreas,
    getAreaById,
    addArea,
    updateAarea,
    getAreasByFilter
}