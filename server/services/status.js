const db = require('../repositories/db');

const getAllStatuses = async (req, res) => {
    db.Get(req, res, 'status');
};

const getStatusById = async (req, res) => {
    db.GetById(req, res, 'status', req.params.id);
};

const addStatus = async (req, res) => {
    db.Add(req, res, 'status', req.body.values);
};

const updateStatus = async (req, res) => {
    db.Put(req, res, 'status', req.params.id, req.body.values);
};

const getstatusesByFilter = (req, res) => {
    db.GetWithFliter(req, res, 'status', req.params.key, req.params.value)
}

module.exports = {
    getAllStatuses,
    getStatusById,
    addStatus,
    updateStatus,
    getstatusesByFilter
}