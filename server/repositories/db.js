var mysql = require('mysql');

var con = mysql.createPool({
    connectionLimit: 100,
    host: 'maayan2020.mysql.database.azure.com',
    user: 'Maayan@maayan2020',
    password: 'Mtms1809',
    database: 'hackathon644',
    debug: false
});

var methods = {};

methods.excQuery = (req, res, query, values) => {
    con.getConnection((err, connection) => {
        if (err) {
            console.log(err);
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        }

        console.log('connected as id ' + connection.threadId);

        connection.query(query, values, 
            function (err, rows) {
                if (!err) {
                    res.json(rows);
                }
            }
        );

        connection.on('error', (err) => {
            console.log(err);
            res.json({ "code": 100, "status": "Error in connection database" });
            return;
        });

        connection.release();
    });
};

methods.Get = (req, res, collectionName) => {
    var query = "SELECT * FROM " + collectionName;
    methods.excQuery(req, res, query);
};

methods.GetById = (req, res, collectionName, id) => {
    var query = "SELECT * FROM " + collectionName + " WHERE id = " + id;
    methods.excQuery(req, res, query);
};

methods.GetWithFliter = (req, res, collectionName, key, value) => {
    var query = "SELECT * FROM " + collectionName + " WHERE " + key + " = " + value;
    methods.excQuery(req, res, query);
};

methods.Add = (req, res, collectionName, values) => {
    var query = "INSERT INTO " + collectionName + " SET ?";
    methods.excQuery(req, res, query, values);
};

methods.Put = (req, res, collectionName, id, values) => {
    var query = "UPDATE " + collectionName + " SET ? WHERE id = " + id;
    methods.excQuery(req, res, query, values);
};

module.exports = methods;