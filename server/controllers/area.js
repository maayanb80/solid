const { area } = require('../services');

const getAllAreas = async (req, res, next) => {
    try {
        area.getAllAreas(req, res);
    } catch (err) {
        next(err);
    }
};

const getAreaById = async (req, res, next) => {
    try {
        area.getAreaById(req, res);
    } catch (err) {
        next(err);
    }
};

const addArea = async (req, res, next) => {
    try {
        area.addArea(req, res);
    } catch (err) {
        next(err);
    }
};

const updateAarea = async (req, res, next) => {
    try {
        area.updateAarea(req, res);
    } catch (err) {
        next(err);
    }
};

const getAreasByFilter = (req, res, next) => {
    try {
        area.getAreasByFilter(req, res);
    } catch (err) {
        next(err);
    }
};

module.exports = {
    getAllAreas,
    getAreaById,
    addArea,
    updateAarea,
    getAreasByFilter
}