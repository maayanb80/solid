const { tasks } = require('../services');

const getAllTasks = (req, res, next) => {
    try {
        tasks.getAllTasks(req, res);
    } catch (err) {
        next(err);
    }
};

const getTaskById = (req, res, next) => {
    try {
        tasks.getTaskById(req, res);
    } catch (err) {
        next(err);
    }
};

const addTask = (req, res, next) => {
    try {
        tasks.addTask(req, res);
    } catch (err) {
        next(err);
    }
};

const updateTask = (req, res, next) => {
    try {
        tasks.updateTask(req, res);
    } catch (err) {
        next(err);
    }
};

const getTasksByUser = (req, res, next) => {
    try {
        tasks.getTasksByUser(req, res);
    } catch (err) {
        next(err);
    }   
}

const getTasksByFilter = (req, res, next) => {
    try {
        tasks.getTasksByFilter(req, res);
    } catch (err) {
        next(err);
    }  
}

module.exports = {
    getAllTasks,
    getTaskById,
    addTask,
    updateTask,
    getTasksByUser,
    getTasksByFilter
}