const { status } = require('../services');

const getAllStatuses = async (req, res, next) => {
    try {
        status.getAllStatuses(req, res);
    } catch (err) {
        next(err);
    }
};

const getStatusById = async (req, res, next) => {
    try {
        status.getStatusById(req, res);
    } catch (err) {
        next(err);
    }
};

const addStatus = async (req, res, next) => {
    try {
        status.addStatus(req, res);
    } catch (err) {
        next(err);
    }
};

const updateStatus = async (req, res, next) => {
    try {
        status.updateStatus(req, res);
    } catch (err) {
        next(err);
    }
};

const getstatusesByFilter = (req, res, next) => {
    try {
        status.getstatusesByFilter(req, res);
    } catch (err) {
        next(err);
    }
};

module.exports = {
    getAllStatuses,
    getStatusById,
    addStatus,
    updateStatus,
    getstatusesByFilter
}