const { users } = require('../services');

const getAllUsers = (req, res, next) => {
    try {
        users.getAllUsers(req, res);
    } catch (err) {
        next(err);
    }
};

const getUserById = (req, res, next) => {
    try {
        users.getUserById(req, res);
    } catch (err) {
        next(err);
    }
};

const addUser = (req, res, next) => {
    try {
        users.addUser(req, res);
    } catch (err) {
        next(err);
    }
};

const updateUser = (req, res, next) => {
    try {
        users.updateUser(req, res);
    } catch (err) {
        next(err);
    }
};

const getUsersByGroup = (req, res, next) => {
    try {
        users.getUsersByGroup(req, res);
    } catch (err) {
        next (err);
    }
}

const getUsersByFilter = (req, res) => {
    try {
        users.getUsersByFilter(req, res);
    } catch (err) {
        next (err);
    }
}

module.exports = {
    getAllUsers,
    getUserById,
    addUser,
    updateUser,
    getUsersByGroup,
    getUsersByFilter
}