const { groups } = require('../services');

const getAllGroups = async (req, res, next) => {
    try {
        groups.getAllGroups(req, res);
    } catch (err) {
        next(err);
    }
};

const getGroupById = async (req, res, next) => {
    try {
        groups.getGroupById(req, res);
    } catch (err) {
        next(err);
    }
};

const addGroup = async (req, res, next) => {
    try {
        groups.addGroup(req, res);
    } catch (err) {
        next(err);
    }
};

const updateGroup = async (req, res, next) => {
    try {
        groups.updateGroup(req, res);
    } catch (err) {
        next(err);
    }
};

const getGroupsByFilter = (req, res, next) => {
    try {
        groups.getGroupsByFilter(req, res);
    } catch (err) {
        next(err);
    }
};

module.exports = {
    getAllGroups,
    getGroupById,
    addGroup,
    updateGroup,
    getGroupsByFilter
}