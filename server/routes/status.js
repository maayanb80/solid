const express = require('express');
const { status } = require('../controllers');

const router = express.Router();

router.get('/', status.getAllStatuses);
router.get('/:id', status.getStatusById);
router.post('/', status.addStatus);
router.put('/:id', status.updateStatus);
router.get('/:key/:value', status.getstatusesByFilter);

module.exports = router;