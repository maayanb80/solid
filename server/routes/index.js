const express = require('express');
const area = require('./area');
const groups = require('./groups');
const status = require('./status');
const tasks = require('./tasks');
const users = require('./users');


const router = express.Router();

router.use('/areas', area);
router.use('/groups', groups);
router.use('/statuses', status);
router.use('/tasks', tasks);
router.use('/users', users);

module.exports = router;