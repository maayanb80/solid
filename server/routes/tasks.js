const express = require('express');
const { tasks } = require('../controllers');

const router = express.Router();

router.get('/', tasks.getAllTasks);
router.get('/:id', tasks.getTaskById);
router.post('/', tasks.addTask);
router.put('/:id', tasks.updateTask);
router.get('/user/:id', tasks.getTasksByUser);
router.get('/:key/:value', tasks.getTasksByFilter);

module.exports = router;