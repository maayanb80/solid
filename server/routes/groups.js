const express = require('express');
const { groups } = require('../controllers');

const router = express.Router();

router.get('/', groups.getAllGroups);
router.get('/:id', groups.getGroupById);
router.post('/', groups.addGroup);
router.put('/:id', groups.updateGroup);
router.get('/:key/:value', groups.getGroupsByFilter);

module.exports = router;