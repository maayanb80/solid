const express = require('express');
const { users } = require('../controllers');

const router = express.Router();

router.get('/', users.getAllUsers);
router.get('/:id', users.getUserById);
router.post('/', users.addUser);
router.put('/:id', users.updateUser);
router.get('/group/:id', users.getUsersByGroup);
router.get('/:key/:value', users.getUsersByFilter);

module.exports = router;