const express = require('express');
const { area } = require('../controllers');

const router = express.Router();

router.get('/', area.getAllAreas);
router.get('/:id', area.getAreaById);
router.post('/', area.addArea);
router.put('/:id', area.updateAarea);
router.get('/:key/:value', area.getAreasByFilter);

module.exports = router;